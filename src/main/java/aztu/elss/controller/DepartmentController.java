package aztu.elss.controller;

import aztu.elss.dto.FacultyDTO;
import aztu.elss.dto.SpecialistDTO;
import aztu.elss.dto.SpecializationDTOdep;
import aztu.elss.service.DepartmentService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/departments")
@CrossOrigin
public class DepartmentController {

    private final DepartmentService departmentService;

    public DepartmentController(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }

    @GetMapping("/faculties")
    public ResponseEntity<List<FacultyDTO>> getAllFaculties() {
        return ResponseEntity.ok(departmentService.getAllFaculties());
    }

    @GetMapping("/faculties/{facultyId}/specialists")
    public ResponseEntity<List<SpecialistDTO>> getSpecialistsByFaculty(@PathVariable Long facultyId) {
        return ResponseEntity.ok(departmentService.getSpecialistsByFaculty(facultyId));
    }

    @GetMapping("/specialists/{specialistId}/specializations")
    public ResponseEntity<List<SpecializationDTOdep>> getSpecializationsBySpecialist(@PathVariable Long specialistId) {
        return ResponseEntity.ok(departmentService.getSpecializationsBySpecialist(specialistId));
    }
}