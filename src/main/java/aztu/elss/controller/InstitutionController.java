package aztu.elss.controller;

import aztu.elss.models.EducationalInstitution;
import aztu.elss.service.InstitutionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/institutions")
@CrossOrigin(origins = "*")
public class InstitutionController {

    private final InstitutionService institutionService;

    public InstitutionController(InstitutionService institutionService) {
        this.institutionService = institutionService;
    }

    @GetMapping
    public List<EducationalInstitution> getAllInstitutions() {
        return institutionService.getAllInstitutions();
    }
}
