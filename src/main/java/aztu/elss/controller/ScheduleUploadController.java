package aztu.elss.controller;

import aztu.elss.service.ScheduleService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/upload")
@CrossOrigin
public class ScheduleUploadController {

    private final ScheduleService scheduleService;

    public ScheduleUploadController(ScheduleService scheduleService) {
        this.scheduleService = scheduleService;
    }
//
//    @PostMapping
//    public ResponseEntity<?> uploadSchedule(@RequestParam("file") MultipartFile file) {
//        if (file.isEmpty()) {
//            return ResponseEntity.badRequest().body("Please upload a non-empty PDF file.");
//        }
//        try {
//            scheduleService.processScheduleFile(file);
//            return ResponseEntity.ok("Schedule processed successfully");
//        } catch (Exception e) {
//            return ResponseEntity.badRequest().body("Failed to process schedule: " + e.getMessage());
//        }
//    }
}