package aztu.elss.controller;


import aztu.elss.dto.GroupDTO;
import aztu.elss.models.ClassEntity;
import aztu.elss.service.GroupService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/groups")
@CrossOrigin(origins = "*")
public class GroupController {

    private final GroupService groupService;

    public GroupController(GroupService groupService) {
        this.groupService = groupService;
    }

//    @GetMapping
//    public List<ClassEntity> getAllGroups() {
//        return groupService.getAllGroups();
//    }

    @GetMapping
    public List<GroupDTO> getAllGroups() {
        return groupService.getAllGroups();
    }

    @GetMapping("/{id}")
    public ClassEntity getGroupById(@PathVariable Long id) {
        return groupService.getGroupById(id);
    }
    @PostMapping
    public ClassEntity createGroup(@RequestBody GroupDTO groupDTO) {
        return groupService.saveGroup(groupDTO);
    }
    @PutMapping("/{id}")
    public ResponseEntity<ClassEntity> updateGroup(@PathVariable Long id, @RequestBody GroupDTO groupDTO) {
        return ResponseEntity.ok(groupService.updateGroup(id, groupDTO));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteGroup(@PathVariable Long id) {
        groupService.deleteGroup(id);
        return ResponseEntity.noContent().build();
    }

}
