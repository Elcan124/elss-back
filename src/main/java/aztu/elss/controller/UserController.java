package aztu.elss.controller;

import aztu.elss.dto.UserDetailsDTO;
import aztu.elss.dto.UserDto;
import aztu.elss.mapper.UserMapper;
import aztu.elss.models.User;
import aztu.elss.models.UserDetails;
import aztu.elss.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/users")
@CrossOrigin
public class UserController {

    private final UserService userService;
    private final UserMapper userMapper;

    public UserController(UserService userService, UserMapper userMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
    }





    @GetMapping()
    public ResponseEntity<List<UserDetailsDTO>> getAllUsers(@RequestParam String role) {
        List<UserDetails> users = userService.findUsersByRole(role);
        List<UserDetailsDTO> userDTOs = userMapper.convertToDtolist(users);
        return new ResponseEntity<>(userDTOs, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDto> findUserById(@PathVariable Long id) {
        UserDto userById = userService.getUserById(id);
        return ResponseEntity.ok(userById);
    }


    @GetMapping
    public ResponseEntity<List<UserDto>> getUserByNameAndSurname(@RequestParam String name ,
                                                                 @RequestParam String surname){
        List<UserDto> userByNameAndSurname = userService.getUserByNameAndSurname(name, surname);

        return ResponseEntity.ok(userByNameAndSurname);
    }


}