package aztu.elss.controller;

import aztu.elss.dto.SpecializationDTO;
import aztu.elss.models.Specialization;
import aztu.elss.service.SpecializationService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Conditional;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
@RestController
@RequestMapping("/specializations") // Move the path here
@RequiredArgsConstructor
@CrossOrigin
public class SpecializationController {

    private final SpecializationService service;

    @GetMapping()
    public ResponseEntity<List<SpecializationDTO>> getAll() {
        List<SpecializationDTO> allSpecializations = service.getAllSpecializations();
        return ResponseEntity.ok(allSpecializations);
    }
}
