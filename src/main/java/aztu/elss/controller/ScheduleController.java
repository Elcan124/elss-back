package aztu.elss.controller;

import aztu.elss.dto.ScheduleDTO;
import aztu.elss.dto.ScheduleViewResponseDto;
import aztu.elss.models.Schedule;
import aztu.elss.service.ScheduleService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/schedule")
@CrossOrigin
public class ScheduleController {


    private final ScheduleService scheduleService;

    public ScheduleController(ScheduleService scheduleService) {
        this.scheduleService = scheduleService;
    }

    @PostMapping
    public Schedule createSchedule(@RequestBody ScheduleDTO scheduleDTO) {
        return scheduleService.saveSchedule(scheduleDTO);
    }
    @GetMapping
    public List<ScheduleViewResponseDto> createSchedule() {
        return scheduleService.getAll();
    }
}