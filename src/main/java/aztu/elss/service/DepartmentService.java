package aztu.elss.service;

import aztu.elss.dto.FacultyDTO;
import aztu.elss.dto.SpecialistDTO;
import aztu.elss.dto.SpecializationDTOdep;
import aztu.elss.mapper.FacultyMapper;
import aztu.elss.mapper.SpecialistMapper;
import aztu.elss.mapper.SpecializationMapper;
import aztu.elss.models.Faculty;
import aztu.elss.models.Specialist;
import aztu.elss.models.Specialization;
import aztu.elss.repository.FacultyRepository;
import aztu.elss.repository.SpecialistRepository;
import aztu.elss.repository.SpecializationRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DepartmentService {


    private final FacultyRepository facultyRepository;

    private final SpecialistRepository specialistRepository;


    private final SpecializationRepo specializationRepository;

    public DepartmentService(FacultyRepository facultyRepository, SpecialistRepository specialistRepository, SpecializationRepo specializationRepository) {
        this.facultyRepository = facultyRepository;
        this.specialistRepository = specialistRepository;
        this.specializationRepository = specializationRepository;
    }

    public List<FacultyDTO> getAllFaculties() {
        return facultyRepository.findAll().stream().map(this::convertToDTO).collect(Collectors.toList());
    }

    public List<SpecialistDTO> getSpecialistsByFaculty(Long facultyId) {
        return specialistRepository.findByFacultyId(facultyId).stream().map(this::convertToDTO).collect(Collectors.toList());
    }

    public List<SpecializationDTOdep> getSpecializationsBySpecialist(Long specialistId) {
        return specializationRepository.findBySpecialistId(specialistId).stream().map(this::convertToDTO).collect(Collectors.toList());
    }

    private FacultyDTO convertToDTO(Faculty faculty) {
//        FacultyDTO dto = new FacultyDTO();
//        dto.setId(faculty.getId());
//        dto.setName(faculty.getName());
//        dto.setDescription(faculty.getDescription());
//        dto.setInstitutionId(faculty.getInstitution().getId());
        return FacultyMapper.INSTANCE.facultyToDto(faculty);
    }

    private SpecialistDTO convertToDTO(Specialist specialist) {

        return SpecialistMapper.INSTANCE.convertToDto(specialist);
//        SpecialistDTO dto = new SpecialistDTO();
//        dto.setId(specialist.getId());
//        dto.setName(specialist.getName());
//        dto.setFacultyId(specialist.getFaculty().getId());
//        return dto;
    }

    private SpecializationDTOdep convertToDTO(Specialization specialization) {
        return SpecializationMapper.INSTANCE.specializationToDTOdep(specialization);
//        SpecializationDTOdep dto = new SpecializationDTOdep();
//        dto.setId(specialization.getId());
//        dto.setName(specialization.getName());
//        dto.setSpecialistId(specialization.getSpecialist().getId());
//        return dto;
    }
}