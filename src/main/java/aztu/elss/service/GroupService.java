package aztu.elss.service;

import aztu.elss.dto.GroupDTO;
import aztu.elss.mapper.GroupMapper;
import aztu.elss.models.ClassEntity;
import aztu.elss.models.EducationalInstitution;
import aztu.elss.models.User;
import aztu.elss.repository.GroupRepository;
import aztu.elss.repository.InstitutionRepository;
import aztu.elss.repository.SpecializationRepo;
import aztu.elss.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class GroupService {

    private final GroupRepository groupRepository;
    private final InstitutionRepository institutionRepository;
    private final UserRepository userRepository;
    private final SpecializationRepo specializationRepo;

    private final GroupMapper groupMapper;

    public GroupService(GroupRepository groupRepository, InstitutionRepository institutionRepository, UserRepository userRepository, SpecializationRepo specializationRepo, GroupMapper groupMapper) {
        this.groupRepository = groupRepository;
        this.institutionRepository = institutionRepository;
        this.userRepository = userRepository;
        this.specializationRepo = specializationRepo;
        this.groupMapper = groupMapper;
    }

    public List<GroupDTO> getAllGroups(){
        List<ClassEntity> all = groupRepository.findAll();
     return   all.stream().map(groupMapper::toGroupDto).toList();
    }

//    public List<ClassEntity> getAllGroups() {
//        List<ClassEntity> classEntity = groupRepository.findAll();
//
//
//        System.out.println("------------------------");
//        System.out.println(classEntity.size());
//      for( ClassEntity classes : classEntity ){
//          classes.setLessons(null);
//          classes.setInstitution(null);
//          classes.setStudents(null);
//          classes.setChief(null);
//          classes.setSpecialization(null);
//      }
//       return classEntity;
//    }

    public ClassEntity getGroupById(Long id) {
        return groupRepository.findById(id).orElseThrow(() -> new RuntimeException("Group not found"));
    }
    public ClassEntity saveGroup(GroupDTO groupDTO) {
        EducationalInstitution institution = institutionRepository.findById(groupDTO.getInstitutionId())
                .orElseThrow(() -> new RuntimeException("Institution not found"));

        User chief = null;
        if (groupDTO.getChiefId() != null) {
            chief = userRepository.findById(groupDTO.getChiefId())
                    .orElseThrow(() -> new RuntimeException("Chief not found"));
        }

        ClassEntity group = ClassEntity.builder()
                .className(groupDTO.getClassName())
                .institution(institution)
                .studentCount(groupDTO.getStudentCount())
                .chief(chief) // This can be null now
                .isActive(groupDTO.getIsActive())
                .instanceDate(LocalDateTime.now())
                .updateDate(LocalDateTime.now())
                .classStartYear(groupDTO.getClassStartYear())
                .classEndYear(groupDTO.getClassEndYear())
                .specialization(specializationRepo.findById(groupDTO.getSpecialtyId()).get())
                .build();

        return groupRepository.save(group);
    }


    public ClassEntity updateGroup(Long id, GroupDTO groupDTO) {
        ClassEntity existingGroup = groupRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Group not found"));



        existingGroup.setClassName(groupDTO.getClassName());

        existingGroup.setStudentCount(groupDTO.getStudentCount());
        existingGroup.setIsActive(groupDTO.getIsActive());
        existingGroup.setClassStartYear(groupDTO.getClassStartYear());
        existingGroup.setClassEndYear(groupDTO.getClassEndYear());
        existingGroup.setUpdateDate(LocalDateTime.now());

        // Handle the chiefId appropriately
        if (groupDTO.getChiefId() != null) {
            User chief = userRepository.findById(groupDTO.getChiefId())
                    .orElseThrow(() -> new RuntimeException("Chief not found"));
            existingGroup.setChief(chief);
        } else {
            existingGroup.setChief(null);
        }

        return groupRepository.save(existingGroup);
    }


    public void deleteGroup(Long id) {
        groupRepository.deleteById(id);
    }

}
