package aztu.elss.service;

import aztu.elss.dto.SpecializationDTO;
import aztu.elss.mapper.SpecializationMapper;
import aztu.elss.models.Specialization;
import aztu.elss.repository.SpecializationRepo;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
public class SpecializationService {

    private final SpecializationRepo specializationRepo;



    public SpecializationService(SpecializationRepo specializationRepo) {
        this.specializationRepo = specializationRepo;

    }

    public List<SpecializationDTO> getAllSpecializations() {
        List<Specialization> all = specializationRepo.findAll();
        return all.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    private SpecializationDTO toDto(Specialization specialization){
      return  SpecializationMapper.INSTANCE.toDTO(specialization);
    }


}