package aztu.elss.service;

import aztu.elss.models.EducationalInstitution;
import aztu.elss.repository.InstitutionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InstitutionService {

    private final InstitutionRepository institutionRepository;

    public InstitutionService(InstitutionRepository institutionRepository) {
        this.institutionRepository = institutionRepository;
    }

    public List<EducationalInstitution> getAllInstitutions() {
        return institutionRepository.findAll();
    }

}
