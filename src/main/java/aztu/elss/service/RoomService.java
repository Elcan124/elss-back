package aztu.elss.service;


import aztu.elss.dto.RoomBuildingDTO;
import aztu.elss.mapper.RoomMapper;
import aztu.elss.models.Room;
import aztu.elss.repository.RoomRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class RoomService {
    private final RoomRepository roomRepository;

    private final RoomMapper roomMapper;

    public RoomService(RoomRepository roomRepository, RoomMapper roomMapper) {
        this.roomRepository = roomRepository;
        this.roomMapper = roomMapper;
    }

    public List<RoomBuildingDTO> getAllRooms() {
        List<Room> rooms = roomRepository.findAll();
       return rooms.stream().map(roomMapper::roomToRoomBuildingDto).collect(Collectors.toList());
//        return rooms.stream()
//                .map(room -> new RoomBuildingDTO(
//                        room.getId(),
//                        room.getRoomNumber(),
//                        room.getBuilding().getBuildingNumber(),
//                        room.getBuilding().getId()
//                ))
//                .collect(Collectors.toList());
    }
}