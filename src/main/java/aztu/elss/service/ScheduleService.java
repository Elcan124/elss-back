package aztu.elss.service;

import aztu.elss.dto.ScheduleDTO;
import aztu.elss.dto.ScheduleViewResponseDto;
import aztu.elss.mapper.ScheduleMapper;
import aztu.elss.models.ClassEntity;
import aztu.elss.models.Lesson;
import aztu.elss.models.Room;
import aztu.elss.models.Schedule;
import aztu.elss.models.User;
import aztu.elss.repository.GroupRepository;
import aztu.elss.repository.LessonRepository;
import aztu.elss.repository.RoomRepository;
import aztu.elss.repository.ScheduleRepository;
import aztu.elss.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class ScheduleService {

    private final ScheduleRepository scheduleRepository;

    private final GroupRepository groupRepository;

    private final LessonRepository lessonRepository;

    private final UserRepository userRepository;

    private final RoomRepository roomRepository;

    private final ScheduleMapper scheduleMapper;

    public ScheduleService(ScheduleRepository scheduleRepository, GroupRepository groupRepository, LessonRepository lessonRepository, UserRepository userRepository, RoomRepository roomRepository, ScheduleMapper scheduleMapper) {
        this.scheduleRepository = scheduleRepository;
        this.groupRepository = groupRepository;
        this.lessonRepository = lessonRepository;
        this.userRepository = userRepository;
        this.roomRepository = roomRepository;
        this.scheduleMapper = scheduleMapper;
    }

    public Schedule saveSchedule(ScheduleDTO scheduleDTO) {
        Schedule schedule = new Schedule();

        ClassEntity group = groupRepository.findById(scheduleDTO.getGroupId())
                .orElseThrow(() -> new RuntimeException("Group not found"));

        Lesson lesson = lessonRepository.findByName(scheduleDTO.getSubject())
                .orElseThrow(() -> new RuntimeException("Subject not found"));

        User teacher = userRepository.findById(scheduleDTO.getTeacherId())
                .orElseThrow(() -> new RuntimeException("Teacher not found"));

        Room room = roomRepository.findById(scheduleDTO.getRoomId())
                .orElseThrow(() -> new RuntimeException("Room not found"));

        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm");
        LocalTime startTime = LocalTime.parse(scheduleDTO.getStartTime(), timeFormatter);
        LocalTime endTime = LocalTime.parse(scheduleDTO.getEndTime(), timeFormatter);

        LocalDate currentDate = LocalDate.now();
        schedule.setStartTime(LocalDateTime.of(currentDate, startTime));
        schedule.setEndTime(LocalDateTime.of(currentDate, endTime));
        schedule.setGroup(group);
        schedule.setLesson(lesson);
        schedule.setTeacher(teacher);
        schedule.setRoom(room);
        schedule.setInstanceDate(LocalDateTime.now());
        schedule.setWeekDayId(scheduleDTO.getWeekDayId());
        schedule.setIsLowerWeek(scheduleDTO.isLowerWeek());
        schedule.setLessonType(scheduleDTO.getLessonType());

        return scheduleRepository.save(schedule);
    }

    public List<ScheduleViewResponseDto> getAll() {
        List<Schedule> all = scheduleRepository.findAll();
        return all.stream().map(this::convertToDTO).collect(Collectors.toList());
    }

    private ScheduleViewResponseDto convertToDTO(Schedule schedule) {

        return scheduleMapper.scheduleToScheduleViewResponseDto(schedule);

//        ScheduleViewResponseDto scheduleDTO = new ScheduleViewResponseDto();


//        scheduleDTO.setGroupName(groupRepository.findById(schedule.getGroup().getId()).get().getClassName());
//        scheduleDTO.setSubject(schedule.getLesson().getName());
//        scheduleDTO.setTeacherName(userRepository.findById(schedule.getTeacher().getId()).get().getUserDetails().getName()+" "+userRepository.findById(schedule.getTeacher().getId()).get().getUserDetails().getSurname());
//        scheduleDTO.setStartTime(schedule.getStartTime().toLocalTime().toString());
//        scheduleDTO.setEndTime(schedule.getEndTime().toLocalTime().toString());
//        scheduleDTO.setWeekDayId(schedule.getWeekDayId());
//        scheduleDTO.setRoomName(roomRepository.findById(schedule.getRoom().getId()).get().getRoomNumber()+"-"+roomRepository.findById(schedule.getRoom().getId()).get().getBuilding().getBuildingName());
//        scheduleDTO.setLowerWeek(schedule.getIsLowerWeek());
//        scheduleDTO.setLessonType(schedule.getLessonType());
//        return scheduleDTO;
    }
}
