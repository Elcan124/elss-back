package aztu.elss.service;

import aztu.elss.dto.UserDetailsDTO;
import aztu.elss.dto.UserDto;
import aztu.elss.mapper.UserMapper;
import aztu.elss.models.User;
import aztu.elss.models.UserDetails;
import aztu.elss.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;


    public UserService(UserRepository userRepository, UserMapper userMapper) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }

    public List<UserDetails> findUsersByRole(String role) {
        List<User> users = userRepository.findUsersByRole(role);
        return users.stream()
                .map(User::getUserDetails)
                .collect(Collectors.toList());
    }

    public UserDto getUserById(Long id) {
        User user = userRepository.findById(id).orElseThrow(() -> new RuntimeException());
        return userMapper.toUserDTO(user);
    }

    public List<UserDto> getUserByNameAndSurname(String name, String surname) {
        List<UserDetails> byUserDetailsNameAndUserDetailsSurname = userRepository.findByUserDetails_NameAndUserDetails_Surname(name, surname);
        List<List<User>> list = byUserDetailsNameAndUserDetailsSurname.stream().map(UserDetails::getUser).toList();
        List<User> list1 = list.stream().map(users -> users.get(0)).toList();
        return list1.stream().map(userMapper::toUserDTO).toList();

    }
}
