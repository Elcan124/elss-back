package aztu.elss.service;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PDFParser {

    public static class Lesson {
        public String group;
        public String weekday;
        public String time;
        public String teacher;
        public String lessonName;
        public String room;
    }

    public List<Lesson> parseTimetable(String filePath) throws IOException {
        List<Lesson> lessons = new ArrayList<>();
        try (PDDocument document = PDDocument.load(new File(filePath))) {
            PDFTextStripper pdfStripper = new PDFTextStripper();
            String text = pdfStripper.getText(document);

            // Split text by lines
            String[] lines = text.split("\\r?\\n");
            Lesson currentLesson = null;
            Pattern groupPattern = Pattern.compile("^[A-Za-z0-9]+ \\(\\d+\\)$");
            Pattern weekdayPattern = Pattern.compile("^(Bazar ertəsi|Çərşənbə axşamı|Çərşənbə|Cümə axşamı|Cümə|Şənbə|Bazar)$");
            Pattern timePattern = Pattern.compile("^\\d{2}:\\d{2}-\\d{2}:\\d{2}$");
            Pattern teacherPattern = Pattern.compile("^[A-Za-zÜŞÇÖĞİıüçöğş. ]+$");
            Pattern lessonNamePattern = Pattern.compile("^[A-Za-zÜŞÇÖĞİıüçöğş. ()\\-]+$");
            Pattern roomPattern = Pattern.compile("^\\d{3}-\\d$");

            for (String line : lines) {
                if (groupPattern.matcher(line).matches()) {
                    if (currentLesson != null) {
                        lessons.add(currentLesson);
                    }
                    currentLesson = new Lesson();
                    currentLesson.group = line.trim();
                } else if (weekdayPattern.matcher(line).matches()) {
                    if (currentLesson != null) {
                        currentLesson.weekday = line.trim();
                    }
                } else if (timePattern.matcher(line).matches()) {
                    if (currentLesson != null) {
                        currentLesson.time = line.trim();
                    }
                } else if (teacherPattern.matcher(line).matches()) {
                    if (currentLesson != null) {
                        currentLesson.teacher = line.trim();
                    }
                } else if (lessonNamePattern.matcher(line).matches()) {
                    if (currentLesson != null) {
                        currentLesson.lessonName = line.trim();
                    }
                } else if (roomPattern.matcher(line).matches()) {
                    if (currentLesson != null) {
                        currentLesson.room = line.trim();
                    }
                }
            }
            if (currentLesson != null) {
                lessons.add(currentLesson);
            }
        }
        return lessons;
    }

    public static void main(String[] args) {
        PDFParser parser = new PDFParser();
        try {
            List<Lesson> lessons = parser.parseTimetable("C:\\Users\\Asus\\OneDrive\\Masaüstü\\ELSS\\ELSS\\qruplar03-10-2023 (3).pdf");
            for (Lesson lesson : lessons) {
                System.out.println("Group: " + lesson.group);
                System.out.println("Weekday: " + lesson.weekday);
                System.out.println("Time: " + lesson.time);
                System.out.println("Teacher: " + lesson.teacher);
                System.out.println("Lesson Name: " + lesson.lessonName);
                System.out.println("Room: " + lesson.room);
                System.out.println("----");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
