package aztu.elss.models;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "classes", schema = "public")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ClassEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "class_name", nullable = false)
    private String className;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "institution_id")
    private EducationalInstitution institution;

    @Column(name = "student_count")
    private Integer studentCount;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "chief_id")
    private User chief;

    @OneToMany(mappedBy = "classEntity", fetch = FetchType.LAZY)
    private List<StudentInfo> students;

    @OneToMany(mappedBy = "classEntity", fetch = FetchType.LAZY)
    private List<ClassLesson> lessons;

    @Column(name = "is_active")
    private Boolean isActive;

    @Column(name = "instance_date")
    private LocalDateTime instanceDate;

    @Column(name = "update_date")
    private LocalDateTime updateDate;

    @Column(name = "class_start_year")
    private Integer classStartYear;

    @Column(name = "class_end_year")
    private Integer classEndYear;


    @ManyToOne
    private Specialization specialization;
}