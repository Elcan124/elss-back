package aztu.elss.models;
import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "permissions", schema = "public")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Permission {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "privilege_name", nullable = false, unique = true)
    private String privilegeName;
}