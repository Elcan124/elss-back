package aztu.elss.models;

import jakarta.persistence.*;
import lombok.*;
import org.mapstruct.Mapper;

import java.util.List;

@Entity
@Table(name = "user_details", schema = "public")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String surname;


    @OneToMany(mappedBy = "userDetails")
    private List <User> user ;

    private String patronymic;

    private java.sql.Date birthdate;

    private String address;
}
