package aztu.elss.models;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "building", schema = "public")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Building {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "campus_id")
    private Campus campus;

    @Column(name = "building_name", nullable = false)
    private String buildingName;

    private String coordinates;

    private String photo;

    @Column(name = "building_number")
    private Integer buildingNumber;
}