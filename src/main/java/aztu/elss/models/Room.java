package aztu.elss.models;

import jakarta.persistence.*;
import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "rooms", schema = "public")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Room {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "building_id")
    private Building building;

    @Column(name = "room_number", nullable = false)
    private String roomNumber;

    @Column(name = "room_max_student_count" )

    private Integer roomMaxStudentCount;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "property_id")
    private Property property;
}