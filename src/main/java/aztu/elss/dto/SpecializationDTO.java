package aztu.elss.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
public class SpecializationDTO {
    private long id;

    private String name;


}
