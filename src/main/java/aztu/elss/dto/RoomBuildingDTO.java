package aztu.elss.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoomBuildingDTO {
    private Long id;
    private String roomNumber;
    private Integer buildingNumber;
    private Long buildingId;

    public RoomBuildingDTO(Long id, String roomNumber, Integer buildingNumber, Long buildingId) {
        this.id = id;
        this.roomNumber = roomNumber;
        this.buildingNumber = buildingNumber;
        this.buildingId = buildingId;
    }
}