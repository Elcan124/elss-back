package aztu.elss.dto;

import lombok.Data;

@Data
public class ScheduleViewResponseDto {
    private String groupName;
    private String subject;
    private String teacherName;
    private String teacherSurname ;
    private String startTime;
    private String endTime;
    private Long weekDayId;
    private String roomName;
    private boolean isLowerWeek;
    private String lessonType;
}
