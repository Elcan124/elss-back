package aztu.elss.dto;

import lombok.Data;

@Data
public class SpecialistDTO {
    private Long id;
    private String name;
    private Long facultyId;
}
