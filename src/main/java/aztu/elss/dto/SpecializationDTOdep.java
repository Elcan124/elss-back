package aztu.elss.dto;

import lombok.Data;

@Data
public class SpecializationDTOdep {
    private Long id;
    private String name;
    private Long specialistId;
}