package aztu.elss.dto;

import lombok.Data;

import java.util.Date;
@Data
public class UserDetailsDTO {
    private Long id;
    private String name;
    private String surname;
    private String patronymic;
    private Date birthdate;
    private String address;


}
