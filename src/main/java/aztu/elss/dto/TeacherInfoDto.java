package aztu.elss.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TeacherInfoDto {
    private Long id;
    private String userName;
    private Long institutionId;
    private Long facultyId;
}

