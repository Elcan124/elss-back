package aztu.elss.dto;

import lombok.Data;

@Data
public class FacultyDTO {
    private Long id;
    private String name;
    private String description;
    private Long institutionId;
}