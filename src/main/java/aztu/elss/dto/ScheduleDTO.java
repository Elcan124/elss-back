package aztu.elss.dto;

import lombok.Data;

@Data
public class ScheduleDTO {
    private Long groupId;
    private String subject;
    private Long teacherId;
    private String startTime;
    private String endTime;
    private Long weekDayId;
    private Long roomId;
    private boolean isLowerWeek;
    private String lessonType;
}