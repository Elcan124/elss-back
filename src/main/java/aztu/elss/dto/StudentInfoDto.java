package aztu.elss.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StudentInfoDto {
    private Long id;

    private String userName;

    private Integer courseNumber;
}
