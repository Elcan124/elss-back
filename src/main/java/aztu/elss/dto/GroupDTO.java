package aztu.elss.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GroupDTO {
    private Long id;
    private String className;
    private Long institutionId;
    private Integer studentCount;
    private Long chiefId;
    private Boolean isActive;
    private Integer classStartYear;
    private Integer classEndYear;

    private long specialtyId;

    private List<StudentInfoDto> students;
}
