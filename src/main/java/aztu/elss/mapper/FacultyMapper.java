package aztu.elss.mapper;

import aztu.elss.dto.FacultyDTO;
import aztu.elss.models.Faculty;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface FacultyMapper {

    FacultyMapper INSTANCE = Mappers.getMapper(FacultyMapper.class);


    @Mapping(source = "institution.id" ,target = "institutionId")
    FacultyDTO facultyToDto(Faculty faculty);


    @Mapping(source = "institutionId" , target = "institution.id")
    Faculty dtoToFaculty(FacultyDTO facultyDTO);

}
