package aztu.elss.mapper;

import aztu.elss.dto.UserDetailsDTO;
import aztu.elss.dto.UserDto;
import aztu.elss.models.Role;
import aztu.elss.models.User;
import aztu.elss.models.UserDetails;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
@Mapper(componentModel = "spring")
public interface UserMapper {



     UserDetailsDTO  convertToDto(UserDetails userDetails);


     default  List<UserDetailsDTO> convertToDtolist(List<UserDetails> userDetailsList){
         return userDetailsList.stream().map(this::convertToDto).collect(Collectors.toList());
     }




    @Mapping(source = "user.userDetails.name", target = "name")
    @Mapping(source = "user.userDetails.surname", target = "surname")
    @Mapping(source = "roles", target = "role" , qualifiedByName = "rolesToRoleNames")
    UserDto toUserDTO(User user);

     @Named(value = "rolesToRoleNames")
    default Set<String> rolesToRoleNames(Set<Role> roles) {
        return roles.stream().map(Role::getName).collect(Collectors.toSet());
    }

//    public static UserDetailsDTO convertToDTO(UserDetails userDetails) {
//        UserDetailsDTO dto = new UserDetailsDTO();
//        dto.setId(userDetails.getId());
//        dto.setName(userDetails.getName());
//        dto.setSurname(userDetails.getSurname());
//        dto.setPatronymic(userDetails.getPatronymic());
//        dto.setBirthdate(userDetails.getBirthdate());
//        dto.setAddress(userDetails.getAddress());
//        return dto;
//    }
//
//    public static List<UserDetailsDTO> convertToDTOs(List<UserDetails> userDetailsList) {
//        return userDetailsList.stream()
//                .map(UserDetailsConverter::convertToDTO)
//                .collect(Collectors.toList());
//    }
}
