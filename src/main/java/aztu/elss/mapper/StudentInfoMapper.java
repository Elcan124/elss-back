package aztu.elss.mapper;

import aztu.elss.dto.StudentInfoDto;
import aztu.elss.models.StudentInfo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface StudentInfoMapper {
    @Mapping(source = "user.username", target = "userName")
    StudentInfoDto toStudentInfoDto(StudentInfo studentInfo);
}
