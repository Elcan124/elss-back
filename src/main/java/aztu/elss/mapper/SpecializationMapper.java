package aztu.elss.mapper;

import aztu.elss.dto.SpecializationDTO;
import aztu.elss.dto.SpecializationDTOdep;
import aztu.elss.models.Specialization;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Component;

@Mapper
public interface SpecializationMapper {

    SpecializationMapper INSTANCE = Mappers.getMapper(SpecializationMapper.class);

//    public

    SpecializationDTO toDTO(Specialization specialization);

    Specialization toEntity(SpecializationDTO dto);

    @Mapping(source = "specialist.id", target = "specialistId")
    SpecializationDTOdep specializationToDTOdep(Specialization specialization);

    @Mapping(source = "specialistId", target = "specialist.id")
    Specialization DtodepToSpecialization(SpecializationDTOdep specializationDTOdep);
//
//    {
//        if (specialization == null) {
//            return null;
//        }
//
//        SpecializationDTO dto = new SpecializationDTO();
//        dto.setId(specialization.getId());
//        dto.setName(specialization.getName());
//        return dto;
//    }

//    public

//    {
//        if (dto == null) {
//            return null;
//        }
//
//        Specialization specialization = new Specialization();
//        specialization.setId(dto.getId());
//        specialization.setName(dto.getName());
//
//        return specialization;
//    }
}