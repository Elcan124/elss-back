package aztu.elss.mapper;

import aztu.elss.dto.GroupDTO;
import aztu.elss.models.ClassEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface GroupMapper {

    @Mapping(source = "institution.id" , target = "institutionId")
    @Mapping(source = "chief.id" , target = "chiefId")
    @Mapping(source = "specialization.specialist.id" , target = "specialtyId")
    @Mapping(source = "students", target = "students")
    GroupDTO toGroupDto(ClassEntity classEntity);
}
