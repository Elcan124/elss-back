package aztu.elss.mapper;

import aztu.elss.dto.ScheduleViewResponseDto;
import aztu.elss.models.Schedule;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;




@Mapper(componentModel = "spring")
public interface ScheduleMapper {

    @Mapping(source = "group.className", target = "groupName")
    @Mapping(source = "lesson.name", target = "subject")
    @Mapping(source = "teacher.userDetails.name", target = "teacherName")
    @Mapping(source = "teacher.userDetails.surname", target = "teacherSurname")
    @Mapping(source = "startTime", target = "startTime", dateFormat = "HH:mm")
    @Mapping(source = "endTime", target = "endTime", dateFormat = "HH:mm")
    @Mapping(source = "weekDayId", target = "weekDayId")
    @Mapping(source = "room.roomNumber", target = "roomName")

    @Mapping(source = "isLowerWeek", target = "lowerWeek")
    @Mapping(source = "lessonType", target = "lessonType")
    ScheduleViewResponseDto scheduleToScheduleViewResponseDto(Schedule schedule);




}





