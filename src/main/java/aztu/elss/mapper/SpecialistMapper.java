package aztu.elss.mapper;

import aztu.elss.dto.SpecialistDTO;
import aztu.elss.models.Specialist;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface SpecialistMapper {

   SpecialistMapper INSTANCE =  Mappers.getMapper(SpecialistMapper.class);


   @Mapping(source = "faculty.id" , target = "facultyId")
   SpecialistDTO convertToDto(Specialist specialist);


   @Mapping(source = "facultyId" , target = "faculty.id")
   Specialist DtoToEntity(SpecialistDTO specialistDTO);
}
