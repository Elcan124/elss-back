package aztu.elss.mapper;

import aztu.elss.dto.RoomBuildingDTO;
import aztu.elss.models.Room;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface RoomMapper {

@Mapping(source = "building.id" ,target = "buildingId")
@Mapping(source = "building.buildingNumber" , target = "buildingNumber")
    RoomBuildingDTO roomToRoomBuildingDto(Room room);
}
