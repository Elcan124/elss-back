package aztu.elss.mapper;

import aztu.elss.dto.TeacherInfoDto;
import aztu.elss.models.TeacherInfo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface TeacherInfoMapper {
    @Mapping(source = "user.userDetails.name", target = "userName")
    @Mapping(source = "institution.id" , target = "institutionId")
    @Mapping(source = "faculty.id" ,target = "facultyId")
    TeacherInfoDto toInfoDto(TeacherInfo teacherInfo);
}
