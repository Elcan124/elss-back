package aztu.elss.enums;

public enum Day {
    MONDAY("Bazar ertesi", 1),
    TUESDAY("Çərşənbə axşamı", 2),
    WEDNESDAY("Çərşənbə", 3),
    THURSDAY("Cümə axşamı", 4),
    FRIDAY("Cümə", 5),
    SATURDAY("Şənbə", 6),
    SUNDAY("Bazar", 7);

    private String azerbaijaniName;
    private int dayNumber;

    Day(String name, int number) {
        this.azerbaijaniName = name;
        this.dayNumber = number;
    }


    public String getAzerbaijaniName() {
        return azerbaijaniName;
    }


    public int getDayNumber() {
        return dayNumber;
    }

    public static Day fromAzerbaijaniName(String name) {
        for (Day day : Day.values()) {
            if (day.getAzerbaijaniName().equalsIgnoreCase(name)) {
                return day;
            }
        }
        throw new IllegalArgumentException("No day with the name: " + name);
    }
}
