package aztu.elss;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class ElssApplication {


    public static void main(String[] args) {
        SpringApplication.run(ElssApplication.class, args);
    }


}
