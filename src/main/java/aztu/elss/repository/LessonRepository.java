package aztu.elss.repository;

import aztu.elss.models.Lesson;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface LessonRepository extends JpaRepository<Lesson  , Long> {
  Optional<Lesson> findByName(String subject);
}
