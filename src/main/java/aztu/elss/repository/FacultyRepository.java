package aztu.elss.repository;

import aztu.elss.models.Faculty;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FacultyRepository extends JpaRepository<Faculty , Long> {
}
