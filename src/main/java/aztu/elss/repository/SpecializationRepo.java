package aztu.elss.repository;

import aztu.elss.models.Specialization;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Arrays;
import java.util.List;

public interface SpecializationRepo  extends JpaRepository<Specialization  ,Long> {
    List<Specialization> findBySpecialistId(Long specialistId);
}
