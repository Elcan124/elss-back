package aztu.elss.repository;

import aztu.elss.models.Specialist;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Arrays;
import java.util.List;

public interface SpecialistRepository extends JpaRepository<Specialist , Long> {


    List<Specialist> findByFacultyId(Long facultyId);
}
