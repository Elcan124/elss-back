package aztu.elss.repository;

import aztu.elss.models.EducationalInstitution;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InstitutionRepository extends JpaRepository<EducationalInstitution , Long> {
}
